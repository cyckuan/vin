# VinID Shop-Next-Month Classifier

This data science model is a LightGBM implementation designed to learn the classification of customers on their propensity to shop or not shop at Rainbow Store next month.


## Technology

* Python
* Google Storage
* Google BigQuery
* Jupyter Notebook
* Tableau Online


## Installation

1. Install all Python packages as per the requirements.txt in the repository


## Access Requirements

### Google Storage
gs://temp-vin/  

### Google Bigquery
project link-249317  
dataset vin  


## Usage


### Data Preparation

1. Add the data file VinIDRecruitChallenge_MLTrack_DataSet.csv to the data folder
2. Execute transfer/convert_data.py to reshape and normalise the data to tabular
3. Execute transfer/load_bigquery.sh to load the reshaped data into BigQuery tables


### Feature Engineering

1. Execute all BigQuery SQL scripts in the order specified in feature_engineering/pipeline.sql
2. Execute transfer/extract_bigquery to extract all model feature datasets from BigQuery
3. Execute transfer/unpack_model_data to download and unpack all model feature datasets into data folder

Execution of BigQuery SQL pipeline can be accelerated by fastquery tool available here:  
https://bitbucket.org/cyckuan/fastquery/


### Model Engineering

1. Run all steps sequentially in the Jupyter notebook model/classification.ipynb


## Contributing

Suggestions and pull requests are welcome.


## License

MIT License

Copyright (c) 2020 Charles Kuan

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
