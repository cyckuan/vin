-- basket and spend rates (normalised by number of days in the period)

%gbq
%dctas vin.vin_fe_transactions_rate
select
*

, baskets_over_15d / 15 as basket_rate_over_15d
, baskets_over_15d_30d / 15 as basket_rate_over_15d_30d
, baskets_over_30d / 30 as basket_rate_over_30d
, baskets_over_30d_60d / 30 as basket_rate_over_30d_60d
, baskets_over_60d / 60 as basket_rate_over_60d

, spend_amt_over_15d / 15 as spend_rate_over_15d
, spend_amt_over_15d_30d / 15 as spend_rate_over_15d_30d
, spend_amt_over_30d / 30 as spend_rate_over_30d
, spend_amt_over_30d_60d / 30 as spend_rate_over_30d_60d
, spend_amt_over_60d / 60 as spend_rate_over_60d

from
vin.vin_fe_transactions_volume
;
