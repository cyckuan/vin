
-- days since last purchase

%gbq
%dctas vin.vin_fe_transactions_days_since_last_purchase

select
csn
, ref_date
, min(days_since_purchase) as days_since_last_purchase

from
vin.vin_base_purchase b

where
ref_date > purchase_date

group by
1,2
;



