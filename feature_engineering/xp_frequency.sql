/*

-- columns

%gbq
%cols vin.vin_challenge_5m
;


-- dimensionality

%gbq
select
count(1) as c
from
vin.vin_challenge_5m
;


-- quick stats

%gbq
select
count(distinct csn) as c_unique_csn
, count(distinct article) as c_unique_article
, min(date) as min_date
, max(date) as max_date
, min(salesquantity) as min_sales
, avg(salesquantity) as avg_sales
, max(salesquantity) as max_sales
, min(price) as min_price
, avg(price) as avg_price
, max(price) as max_price
from
vin.vin_challenge_5m
;





-- items bought once

%gbq

%dctas vin.vin_low_freq_article

select
article

from
(

select
csn
, article
, count(distinct date) as buy_frequency

from
(
select
csn
, date
, article
from
vin.vin_challenge_5m
group by
1,2,3
)

group by
1,2

having
count(distinct date) = 1

)

group by
1
;

%gbq
%scf article_bought_once
vin.vin_low_freq_article
;




-- customers shopped once

%gbq

%dctas vin.vin_low_freq_csn

select
csn

from
(
select
csn
, date
from
vin.vin_challenge_5m
group by
1,2
)

group by
csn

having
count(distinct date) = 1

;


%gbq
%scf csn_bought_once
vin.vin_low_freq_csn
;








-- article histogram
-- number of unique customers that bought article


%gbq

select
article
, count(distinct csn) as c_csn

from
vin.vin_challenge_5m

group by
1

order by
2 desc
;




-- customer histogram
-- most frequent shoppers by date

%gbq

select
csn
, count(distinct date) as c_u_date
, count(distinct article) as c_u_article

from
vin.vin_challenge_5m

group by
1

order by
2 desc
;


-- persons who bought only 1 article and higher

%gbq

select
c_u_article
, count(distinct csn) as c_csn

from
(

select
csn
, count(distinct date) as c_u_date
, count(distinct article) as c_u_article

from
vin.vin_challenge_5m

group by
1

)

group by
1

order by
1

;



-- customer basket size
-- avergage number of different products in each basket

%gbq

select
min(c_article) as min_basket_size
, avg(c_article) as avg_basket_size
, max(c_article) as max_basket_size

from
(

select
csn
, date
, count(distinct article) as c_article
from
vin.vin_challenge_5m
group by
1,2

)
;




-- article freq csn

%gbq

select
c_article
, count(1) as c_freq_csn

from
(

select
csn
, count(distinct article) as c_article
from
vin.vin_challenge_5m
group by
1

)

group by
1

order by
1

;



-- article freq csn

%gbq

select
c_article
, count(1) as c_freq_csn_date

from
(

select
csn
, date
, count(distinct article) as c_article
from
vin.vin_challenge_5m
group by
1,2

)

group by
1

order by
1

;



*/




%gbq

select
single_purchase
, count(distinct article) as c_article

from
(

select
article
, case when low.article is null then 'n' else 'y' end as single_purchase

from
(
select
article
from
vin.vin_challenge_5m
group by
1
) a

left join
(
select
article
from
vin.vin_low_freq_article
) low
using (article)

)

group by
1
;




%gbq

select
single_purchase
, count(distinct csn) as c_csn

from
(

select
csn
, case when low.csn is null then 'n' else 'y' end as single_purchase

from
(
select
csn
from
vin.vin_challenge_5m
group by
1
) a

left join
(
select
csn
from
vin.vin_low_freq_csn
) low
using (csn)

)

group by
1
;
