
-- purchase recency

%gbq
%dctas vin.vin_fe_transactions_recency

select
csn
, ref_date
, case when days_since_last_purchase <= 15 then 1 else 0 end as purchased_in_last_15d
, case when days_since_last_purchase <= 30 then 1 else 0 end as purchased_in_last_30d
, case when days_since_last_purchase <= 60 then 1 else 0 end as purchased_in_last_60d

from
vin.vin_fe_transactions_days_since_last_purchase
;

