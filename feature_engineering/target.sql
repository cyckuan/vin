-- identify csn and corresponding purchase months

%gbq

%dctas vin.vin_target_purchase_month
select
csn
, extract(month from purchase_date) as purchase_month
from
vin.vin_base
group by

1,2
;


-- overview of purchasers in each month

%gbq
select
purchase_month
, count(distinct csn) as c_csn
from
vin.vin_target_purchase_month
group by
1
order by
1
;



-- identify target and predictor sets for each reference date

%gbq
%dctas vin.vin_target_predict_feature_sets

select
csn
, ref_date
, predict_month
, case when sum(predict) > 0 then 1 else 0 end as predict
, case when sum(feature) > 0 then 1 else 0 end as feature

from
(

select
t.csn
, ref.ref_date
, ref.predict_month
, case when purchase_month = ref.predict_month then 1 else 0 end as predict
, case when purchase_month < ref.predict_month then 1 else 0 end as feature

from
vin.vin_target_purchase_month t

cross join
vin.vin_ref_date ref

)

where
predict > 0
or feature > 0

group by
1,2,3
;



-- overview of feature sets

%gbq
select
ref_date
, predict_month
, predict
, feature
, count(distinct csn) as c_csn

from
vin.vin_target_predict_feature_sets

group by
1,2,3,4

order by
1,2,3,4

limit
100
;



-- identify positive and negative class
-- positive class are those who bought in prior periods and prediction month
-- negative class are those who bought in prior periods but did not buy in prediction month

%gbq
%dctas vin.vin_target_positive_negative_class

select
csn
, ref_date
, predict_month
, case
when predict = 1 and feature = 1 then 1
when predict = 0 and feature = 1 then 0
else -1
end as target_class
from
vin.vin_target_predict_feature_sets
where
(predict = 1 and feature = 1)
or (predict = 0 and feature = 1)
group by
1,2,3,4
;



-- overview of number of customers in each class and reference dates

%gbq
select
ref_date
, predict_month
, target_class
, count(distinct csn) as c_csn
from
vin.vin_target_positive_negative_class
group by
1,2,3
order by
1,2,3
limit
100
;
