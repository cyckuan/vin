-- combine target variable and all feature sets to build consolidated model data

%gbq
%dctas vin.vin_model_all

select
t.*

, dslp.days_since_last_purchase

, rc.purchased_in_last_15d
, rc.purchased_in_last_30d
, rc.purchased_in_last_60d

, v.baskets_over_15d
, v.baskets_over_30d
, v.baskets_over_60d
, v.baskets_over_15d_30d
, v.baskets_over_30d_60d

, v.spend_amt_over_15d
, v.spend_amt_over_30d
, v.spend_amt_over_60d
, v.spend_amt_over_15d_30d
, v.spend_amt_over_30d_60d

, r.basket_rate_over_15d
, r.basket_rate_over_30d
, r.basket_rate_over_60d
, r.basket_rate_over_15d_30d
, r.basket_rate_over_30d_60d

, r.spend_rate_over_15d
, r.spend_rate_over_30d
, r.spend_rate_over_60d
, r.spend_rate_over_15d_30d
, r.spend_rate_over_30d_60d

, m.basket_momentum_short
, m.basket_momentum_long
, m.spend_momentum_short
, m.spend_momentum_long

from
vin.vin_target_positive_negative_class t

join
vin.vin_fe_transactions_days_since_last_purchase dslp
using (csn, ref_date)

join
vin.vin_fe_transactions_recency rc
using (csn, ref_date)

join
vin.vin_fe_transactions_volume v
using (csn, ref_date)

join
vin.vin_fe_transactions_rate r
using (csn, ref_date)

join
vin.vin_fe_transactions_momentum m
using (csn, ref_date)

;
