-- purchase aggregate by csn and date
-- mapped to reference dates

%gbq
%dctas vin.vin_base_purchase

select
csn
, ref_date
, purchase_date
, date_diff(ref.ref_date, b.purchase_date, day) as days_since_purchase
, total_qty
, total_amt

from
(
select
csn
, purchase_date
, sum(qty) as total_qty
, sum(amt) as total_amt

from
vin.vin_base

group by
1,2
) b

cross join
vin.vin_ref_date ref

where
ref.ref_date > b.purchase_date
;

