-- set up reference dates

%gbq
create or replace table vin.vin_ref_date (
ref_date date
, predict_month int64
)
;

%gbq
insert into vin.vin_ref_date values
('2018-03-31',4)
, ('2018-04-30',5)
, ('2018-05-31',6)
, ('2018-06-30',7)
;

%gbq
%sf
vin.vin_ref_date
;
