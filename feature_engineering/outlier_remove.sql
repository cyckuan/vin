%gbq
delete
from
vin.vin_base b
where
exists
(
select
* from
vin.vin_outlier_exceptions o
where
b.csn = o.csn
and b.purchase_date = o.purchase_date
and b.article = o.article
)
;

