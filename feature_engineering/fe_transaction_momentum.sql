-- momentum i.e. change in basket and spend rates

%gbq
%dctas vin.vin_fe_transactions_momentum
select
*
, ln(basket_rate_over_15d + 1) - ln(basket_rate_over_15d_30d + 1) as basket_momentum_short
, ln(basket_rate_over_30d + 1) - (basket_rate_over_30d_60d + 1) as basket_momentum_long
, ln(spend_amt_over_15d + 1) - (spend_amt_over_15d_30d + 1) as spend_momentum_short
, ln(spend_amt_over_30d + 1) - (spend_amt_over_30d_60d + 1) as spend_momentum_long
from
vin.vin_fe_transactions_rate
;
