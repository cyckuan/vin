%gbq
%dctas vin.vin_fe_products_popularity

select
article
, ref_date

, ln(qty_over_15d + 1) - ln(qty_over_15d_30d + 1) as article_qty_popularity_short
, ln(qty_over_30d + 1) - ln(qty_over_30d_60d + 1) as article_qty_popularity_long

, ln(baskets_over_15d + 1) - ln(baskets_over_15d_30d + 1) as article_baskets_popularity_short
, ln(baskets_over_30d + 1) - ln(baskets_over_30d_60d + 1) as article_baskets_popularity_long

from
(

select
*

, baskets_over_30d - baskets_over_15d as baskets_over_15d_30d
, baskets_over_60d - baskets_over_30d as baskets_over_30d_60d

, qty_over_30d - qty_over_15d as qty_over_15d_30d
, qty_over_60d - qty_over_30d as qty_over_30d_60d

from
(

select
article
, ref_date

, sum(case when days_since_purchase < 15 then 1 else 0 end) as baskets_over_15d
, sum(case when days_since_purchase < 30 then 1 else 0 end) as baskets_over_30d
, sum(case when days_since_purchase < 60 then 1 else 0 end) as baskets_over_60d

, sum(case when days_since_purchase < 15 then qty else 0 end) as qty_over_15d
, sum(case when days_since_purchase < 30 then qty else 0 end) as qty_over_30d
, sum(case when days_since_purchase < 60 then qty else 0 end) as qty_over_60d

from
(

select
article
, ref.ref_date
, purchase_date
, date_diff(ref.ref_date, b.purchase_date, day) as days_since_purchase
, qty
, amt

from
vin.vin_base b

cross join
vin.vin_ref_date ref

)

group by
1,2

)

)
;
