-- volume over period

%gbq

%dctas vin.vin_fe_transactions_volume

select
*
, baskets_over_30d - baskets_over_15d as baskets_over_15d_30d
, baskets_over_60d - baskets_over_30d as baskets_over_30d_60d
, spend_amt_over_30d - spend_amt_over_15d as spend_amt_over_15d_30d
, spend_amt_over_60d - spend_amt_over_30d as spend_amt_over_30d_60d

from
(

select
csn
, ref_date
, sum(case when days_since_purchase < 15 then 1 else 0 end) as baskets_over_15d
, sum(case when days_since_purchase < 30 then 1 else 0 end) as baskets_over_30d
, sum(case when days_since_purchase < 60 then 1 else 0 end) as baskets_over_60d
, sum(case when days_since_purchase < 15 then total_amt else 0 end) as spend_amt_over_15d
, sum(case when days_since_purchase < 30 then total_amt else 0 end) as spend_amt_over_30d
, sum(case when days_since_purchase < 60 then total_amt else 0 end) as spend_amt_over_60d

from
vin.vin_base_purchase

group by
1,2

)
;

