-- set reference dates

config_reference_dates.sql

-- prepare base outlier with data cleansing and outlier treatment

base.sql
outlier.sql
outlier_remove.sql
-- outlier_survey.sql


-- feature engineering for each reference date

fe_transaction_base.sql
fe_transaction_dslp.sql
fe_transaction_recency.sql
fe_transaction_volume.sql
fe_transaction_rate.sql
fe_transaction_momentum.sql

-- fe_product_popularity.sql

-- develop target data for each reference date

target.sql

-- prepare modelling data

generate_model_data.sql
-- generate_partitions.sql
generate_datasets.sql

