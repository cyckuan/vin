%gbq

select
article
, avg(price) as avg_price
, stddev_pop(price) as stddev_price
, stddev_pop(price)/avg(price) as relative_dev
, count(1) as c_baskets
, sum(qty) as s_qty
from
vin.vin_base
group by
1
order by
4 desc
limit
100

;
