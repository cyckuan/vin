-- base data

-- aggregation to group same day same article transactions, thereby rmoving corrections


%gbq

%dctas vin.vin_base
select
*
, ln(qty) as log_qty
, ln(price) as log_price
, ln(amt) as log_amt
from
(
select
csn
, cast(date as date) as purchase_date
, article
, sum(salesquantity) as qty
, avg(price) as price
, sum(salesquantity * price) as amt
from
vin.vin_challenge_5m
group by
1,2,3
)
;

