
-- pre-outlier-treatment quantity stats for outlier identification

%gbq
%dctas vin.vin_outlier_stats

select
avg(qty) as avg_qty
, avg(log_qty) as avg_log_qty
, stddev_pop(log_qty) as stddev_log_qty

from
vin.vin_base

where
qty >= 1
and price > 0

;



-- pre-outlier-treatment article-specific price and quantity stats for outlier identification

%gbq
%dctas vin.vin_outlier_stats_article

select
article

, avg(qty) as avg_qty
, avg(log_qty) as avg_log_qty
, stddev_pop(log_qty) as stddev_log_qty

, avg(price) as avg_price
, avg(log_price) as avg_log_price
, stddev_pop(log_price) as stddev_log_price

, avg(amt) as avg_amt
, avg(log_amt) as avg_log_amt
, stddev_pop(log_amt) as stddev_log_amt

from
vin.vin_base

where
qty >= 1
and price > 0

group by
1
;



-- identify outliers as greater than 3 standard deviations from log statistics

%gbq
%dctas vin.vin_outlier_exceptions

select
case
when log_qty > (o.avg_log_qty + 3 * o.stddev_log_qty) then 'q'
when log_price > (o.avg_log_price + 3 * o.stddev_log_price) then 'p'
else 'o'
end as reason
, csn
, purchase_date
, b.article
, qty
, log_qty
, o.avg_log_qty
, o.stddev_log_qty
, price
, log_price
, avg_log_price
, stddev_log_price

from
vin.vin_base b

cross join
vin.vin_outlier_stats og

join
vin.vin_outlier_stats_article o
on
b.article = o.article

where
o.stddev_log_qty > 0
and o.stddev_log_price > 0
and
(
log_qty > (o.avg_log_qty + 3 * o.stddev_log_qty)
or log_price > (o.avg_log_price + 3 * o.stddev_log_price)
)
;
