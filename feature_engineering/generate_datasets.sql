-- split consolidated data into various partitions


-- training

%gbq
%dctas vin.vin_model_train
select
m.*
from
vin.vin_model_all m
join
vin.vin_model_partitions p
using (csn, ref_date)
where
ref_date = '2018-03-31'
;

-- development

%gbq
%dctas vin.vin_model_dev
select
m.*
from
vin.vin_model_all m
join
vin.vin_model_partitions p
using (csn, ref_date)
where
ref_date = '2018-04-30'
;

-- holdout

%gbq
%dctas vin.vin_model_holdout
select
m.*
from
vin.vin_model_all m
join
vin.vin_model_partitions p
using (csn, ref_date)
where
ref_date = '2018-05-31'
;

-- training + development

%gbq
%dctas vin.vin_model_train_dev
select
m.*
from
vin.vin_model_all m
join
vin.vin_model_partitions p
using (csn, ref_date)
where
ref_date = '2018-03-31'
or ref_date = '2018-04-30'
;

