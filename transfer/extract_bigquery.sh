#!/usr/bin/env bash

# extracts model data from BigQuery

bq --location=US extract \
--destination_format CSV \
--compression GZIP \
--print_header=True \
link-249317:vin.vin_model_train \
gs://temp-vin/ckuan/model_train.csv.gz

bq --location=US extract \
--destination_format CSV \
--compression GZIP \
--print_header=True \
link-249317:vin.vin_model_dev \
gs://temp-vin/ckuan/model_dev.csv.gz

bq --location=US extract \
--destination_format CSV \
--compression GZIP \
--print_header=True \
link-249317:vin.vin_model_holdout \
gs://temp-vin/ckuan/model_holdout.csv.gz

bq --location=US extract \
--destination_format CSV \
--compression GZIP \
--print_header=True \
link-249317:vin.vin_model_train_dev \
gs://temp-vin/ckuan/model_train_dev.csv.gz

bq --location=US extract \
--destination_format CSV \
--compression GZIP \
--print_header=True \
link-249317:vin.vin_model_all \
gs://temp-vin/ckuan/model_all.csv.gz
