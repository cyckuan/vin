#!/usr/bin/env bash

### Loads parquet data to BigQuery


# parameters

filepath="../data"
filename="VinIDRecruitChallenge_MLTrack_DataSet.csv.parquet"
gs_path="gs://temp-vin/ckuan"
bq_table="vin.vin_challenge_5m"


# upload to Google Storage

gsutil -m cp $filepath/$filename $gs_path/


# load into BigQuery

bq load \
--source_format=PARQUET \
$bq_table \
$gs_path/$filename
