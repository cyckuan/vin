#!/usr/bin/env bash

# download extracted model data from cloud storage to local and unpack

rm ../data/*.csv.gz

gsutil cp gs://temp-vin/ckuan/model_train.csv.gz ../data/
gzip -d ../data/model_train.csv.gz

gsutil cp gs://temp-vin/ckuan/model_dev.csv.gz ../data/
gzip -d ../data/model_dev.csv.gz

gsutil cp gs://temp-vin/ckuan/model_holdout.csv.gz ../data/
gzip -d ../data/model_holdout.csv.gz

gsutil cp gs://temp-vin/ckuan/model_train_dev.csv.gz ../data/
gzip -d ../data/model_train_dev.csv.gz

gsutil cp gs://temp-vin/ckuan/model_all.csv.gz ../data/
gzip -d ../data/model_all.csv.gz

