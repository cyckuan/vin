#!/usr/bin/env python3

"""
Reshapes data file from uneven list to long format and key-value to columnar

Arguments:
FILENAME (inline constant) - name of the data file to reshape

"""

# parameters

FILENAME = '../data/VinIDRecruitChallenge_MLTrack_DataSet.csv'


# packages

import vaex as vx
import pandas as pd


# read file into Pandas

df = pd.read_csv(FILENAME)

# test code
# df = df.head()

print('pre-conversion shape:\n' + str(df.shape))


# reshape data

df =  ( df.set_index(['csn','date']).transaction_info
        .apply(lambda x: eval(x))
        .explode()
        .apply(pd.Series)
        .reset_index()
      )

"""
from a data science perspective, the pandas data wrangling functions are elegant
but from an engineering perspective, this code will need to be modified
to process incrementally when dealing with larger datasets not fully loadable into memory

"""

print('post-conversion shape:\n' + str(df.shape))


# save data in parquet

vx_df = vx.from_pandas(df)
vx_df.export_parquet(FILENAME+'.parquet')

